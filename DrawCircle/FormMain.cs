﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace DrawCircle
{
    public partial class FormMain : System.Windows.Forms.Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            try
            {
                int x = Int32.Parse(textBoxX.Text);
                int y = Int32.Parse(textBoxY.Text);
                int radius = int.Parse(textBoxRadius.Text);

                Pen pen = new Pen(Color.Blue);

                SolidBrush brush = new SolidBrush(Color.Green);

                Graphics graphics = panelMain.CreateGraphics();

                if (checkBoxToKhali.Checked)
                { graphics.DrawCircle(pen, x, y, radius); }
                else
                { graphics.FillCircle(brush, x, y, radius); }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }
    }
}
