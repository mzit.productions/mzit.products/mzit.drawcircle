﻿using System.Drawing;

namespace DrawCircle
{
    public static class GraphicsExtensions
    {
        public static void DrawCircle(
            this Graphics graphics, Pen pen,
            float x, float y, float radius)
        {
            graphics.DrawEllipse(pen, x - radius, y - radius,
                radius + radius, radius + radius);
        }

        public static void FillCircle(
            this Graphics graphics, Brush brush,
            float x, float y, float radius)
        {
            graphics.FillEllipse(brush, x - radius, y - radius,
                radius + radius, radius + radius);
        }
    }
}
